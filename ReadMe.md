# SCC Common Data API Downloader #

**Version 1.0.0**

The program, on input a json file, downloads from the common data API all the data matching the given subject ids, measure types and time range, creating a local dataset

# Requirements #
To run the script one needs python (version >= 3.0) and the pip3 installer, which can be used to download the requirements via the command
> pip3 install -r requirements.txt

# Input file #
Create an input file in json format, containing the following fields (see file input.json in folder data for an example):
- "endpoint": the endpoint of Common Data API
- "user" and "password" to access to Common Data API
- "subject_ids": list of subjects to download data
- "measure_types": list of datatypes to download
- "from" and "until", as ISOformat string, defining the time range of the data to download
It is also possible to specify in the input json the "output_folder" where to store the downloaded_data (if not specified by default the program creates a "downoladed_data" folder) and the format of downloaded data. Available formats are 'json', creating a single json file for each measurement, 'json_list' and 'csv', creating for each subject a single "measurements" file containing all the subject measurements. If not specified, 'json' is the default format

# Program usage #
Launch the program specifying the path of the json file with the flag -i. 

Example:
> python3 downloader.py -i data/input.json


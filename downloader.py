##      Version 1.0.0.        ##

import pandas as pd
import argparse
import requests
import random
import os
import base64

from json import load, dumps, loads

def create_output_folder(out_folder):
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    else:
        raise DuplicateOutputFolder("The folder %s already exists. Choose a different name for the output folder" % out_folder)

def read_and_check_input_json(json_file):
    with open(json_file, 'r') as file:
        input_json = load(file)

    if "endpoint" not in input_json:
        raise BadInput("Endpoint missing in the input json file!")
    if "user" not in input_json:
        raise BadInput("User missing in the input json file!")
    if "password" not in input_json:
        raise BadInput("Password missing in the input json file!")
    if "subject_ids" not in input_json:
        raise BadInput("List of subject ids missing in the input json file!")
    if len(input_json["subject_ids"]) == 0:
        raise BadInput("Empty list of subjet given!")
    if "measure_types" not in input_json:
        raise BadInput("List of measure types missing in the input json file!")
    if len(input_json["measure_types"]) == 0:
        raise BadInput("Empty list of types given!")
    if "output_format" not in input_json:
        input_json["output_format"] = "json"
    if input_json["output_format"] not in ["json", "json_list", "csv"]:
        raise BadInput("Not admitted output format. Available formats are 'json', 'json_list' and 'csv'")
    if "output_folder" not in input_json:
        input_json["output_folder"] = "./downloaded_data"

    return input_json

def require_list_measures(endpoint, auth, patient_id, measure_type, datetimes):
    page_size = 10
    page_number = 1
    total = None

    measure_ids = []
    while total is None or page_number <= total / page_size:
        url_request = endpoint + "/" + str(patient_id)
        args = [("measure_type", measure_type),
                ("from_timestamp", datetimes[0]),
                ("until_timestamp", datetimes[1]),
                ("page_size", page_size),
                ("page_number", page_number)]
        measure_list = requests.get(url_request, params=args, auth=auth)

        if measure_list.status_code != 200:
            print("Error while requiring measure list at url: %s" % url_request)
            print(measure_list.text)
            raise Exception("Error while requiring measure list")

        else:
            measure_list = measure_list.json()
            total = measure_list["total"]

            for measure in measure_list["data"]:
                measure_ids.append(measure["measurement_id"])

            page_number += 1

    if len(measure_ids) != len(set(measure_ids)):
        measure_ids = list(dict.fromkeys(measure_ids))

    return measure_ids

def store_measure_list(measure_list, endpoint, auth, subject_id, subject_folder, out_format):
    print("Measure_list size:", len(measure_list))
    if len(set(measure_list)) != len(measure_list):
        print("Different elements in list:", len(set(measure_list)))
        measure_list = list(set(measure_list))
    total = len(measure_list)
    stored = 0

    
    downloaded_measure_list = []
    # time_download = time()
    for measurement_id in measure_list:
        downloaded_measure_list.append(download_measurement(endpoint, auth, subject_id, measurement_id))
        stored += 1
        print("Measure downloaded: %d of %d" % (stored, total), end='\r')
    # print("Time downloading measures: %d ms" % int( 1000 * (time()-time_download)))
    for measurement in downloaded_measure_list:
        try:
            measurement["measurement_content"] = loads(base64.b64decode(measurement["measurement_content"].encode('ascii')))
        except KeyError:
            measurement["measurement_content"] = loads(base64.b64decode(measurement["measurement_data"].encode('ascii')))

    if out_format == "json":
        index = 0
        for measurement in downloaded_measure_list:
            fname = subject_folder + "/" + "{:04d}".format(index) + " - " + measurement["measurement_mediatype"].split("/")[1].split("+")[0]
            ext = measurement["measurement_mediatype"].split("+")[1]
            with open(fname + "." + ext, "wb") as outfile:
                outfile.write(measurement["measurement_content"])

            with open(fname + "_metadata.json", "w") as outfile:
                _metadata = measurement
                _metadata["measurement_content"] = None
                outfile.write(dumps(_metadata))
            index+=1
        print("Store measures in folder %s" % subject_folder)

    elif out_format == "json_list":
        filename = subject_folder + "/measures.json"
        print("Store measures in file %s" % filename)
        with open(filename, 'w') as file:
            file.write(dumps(downloaded_measure_list))

    elif out_format == "csv":
        filename = subject_folder + "/measures.csv"
        print("Store measures in file %s" % filename)
        data = pd.DataFrame(downloaded_measure_list)
        with open(filename, 'w', newline='') as file:
            file.write(data.to_csv(index=False))

    return

def download_measurement(endpoint, auth, patient_id, measurement_id):
    url_request = endpoint + "/" + str(patient_id) + "/" + measurement_id
    measurement = requests.get(url_request, auth=auth)
    if measurement.status_code != 200:
        print("Error while requiring measurement at url: %s" % url_request)
        print(measurement.text)
        raise Exception("Error while requiring measure list")
    else:
        return measurement.json()

class BadInput(Exception):
    pass

class DuplicateOutputFolder(Exception):
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_json', type=str, required=True, help='Path of the input json file')
    args = parser.parse_args()

    print("Input parsed")
    # Prende il json di input, lo legge e verifica sia ben formattato, sollevando un errore altrimenti.
    input_json = read_and_check_input_json(args.input_json)
    endpoint = input_json["endpoint"]
    datetimes = (input_json["from"], input_json["until"])

    create_output_folder(input_json["output_folder"])

    auth=requests.auth.HTTPBasicAuth(input_json["user"], input_json["password"])

    subject_list = input_json["subject_ids"]
    random.shuffle(subject_list)

    i = 1
    for subject_id in subject_list:
        print("Subject %d of %d" % (i, len(subject_list)))

        anonymous_id = "Subject " + "{:03d}".format(i)

        subject_folder = input_json["output_folder"] + "/" + anonymous_id

        if not os.path.exists(subject_folder):
            os.makedirs(subject_folder)

        measure_list = []
        for measure_type in input_json["measure_types"]:
            measure_list += require_list_measures(endpoint, auth, subject_id, measure_type, datetimes)

        store_measure_list(measure_list, endpoint, auth, subject_id, subject_folder, input_json["output_format"])

        i += 1

import pytest
import os
import shutil
import pandas as pd

@pytest.fixture
def output_folder():
    output_folder = "/tmp/test_downloader"
    if os.path.exists(output_folder):
        shutil.rmtree(output_folder)

    return output_folder

@pytest.mark.acceptance
def test_downloader_csv(output_folder):
    # given: a valid path for the input json where the specified output format is "csv"
    input_json = "./test_json/csv.json"
    
    # when: requiring to run the downloader
    os.system('python3 downloader.py -i %s' % (input_json))

    # then: the output folders are correctly created
    assert os.path.isdir(output_folder)
    assert os.path.isdir(output_folder + '/Subject 001')
    # the file in the output folder is as expected
    assert len(os.listdir(output_folder + '/Subject 001')) == 1
    assert os.listdir(output_folder + '/Subject 001')[0] == "measures.csv"

    # when: reading the csv file in a pandas DataFrame:
    data = pd.read_csv(output_folder + '/Subject 001/measures.csv')

    # then: the data are as expected:
    assert len(data) == 200

@pytest.mark.acceptance
def test_downloader_json_list(output_folder):
    # given: a valid path for the input json where the specified output format is "csv"
    input_json = "./test_json/json_list.json"
    
    # when: requiring to run the downloader
    os.system('python3 downloader.py -i %s' % (input_json))

    # then: the output folders are correctly created
    assert os.path.isdir(output_folder)
    assert os.path.isdir(output_folder + '/Subject 001')
    # the file in the output folder is as expected
    assert len(os.listdir(output_folder + '/Subject 001')) == 1
    assert os.listdir(output_folder + '/Subject 001')[0] == "measures.json"

@pytest.mark.acceptance
def test_downloader_json(output_folder):
    # given: a valid path for the input json without specified output format
    input_json = "./test_json/default.json"
    output_folder = "/tmp/test_downloader"
    if os.path.exists(output_folder):
        shutil.rmtree(output_folder)

    assert not os.path.isdir(output_folder)

    # when: requiring to run the downloader with the selected json, 
    os.system('python3 downloader.py -i %s' % (input_json))

    # then: the output folders are correctly created
    assert os.path.isdir(output_folder)
    assert os.path.isdir(output_folder + '/Subject 001')
    # the files in the output folder are as expected, using the default output format (json)
    assert len(os.listdir(output_folder + '/Subject 001')) == 1
    assert os.listdir(output_folder + '/Subject 001')[0].split('.')[-1] == "json"